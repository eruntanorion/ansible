Private Ansible playbooks

Get this repo:
git clone git@gitlab.com:eruntanorion/ansible.git

Apply changes:
git push -u origin master

Run the playbooks:
sudo ansible-pull -U https://gitlab.com/eruntanorion/ansible.git

